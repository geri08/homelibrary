package com.example.home.books;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookDAO {

    @Autowired
    private BookRepository bookRepository;


    public List<BookDTO> findBooks() {
        return bookRepository.findAllBook();
    }

    public void addBook(Book book) {
        bookRepository.save(book);
    }

    public void editBook(Long id, String title, String author) {

        Optional<Book> bookRepositoryById = bookRepository.findById(id);
        bookRepositoryById.toString();
    }
}
