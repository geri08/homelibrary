package com.example.home.books;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookDAO bookDAO;

    public List<BookDTO> findBooks() {
        return bookDAO.findBooks();
    }

    public void addbook(String title, String author) {
        Book book = new Book(title,author);
    bookDAO.addBook(book);
    }

    public void editbook(Long id, String title, String author) {
bookDAO.editBook(id,title,author);
    }
}
