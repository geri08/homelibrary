package com.example.home.books;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public String listOfAllBooks(Model model) {
        model.addAttribute("books", bookService.findBooks());
        return "books";
    }


    @GetMapping("/addbook")
    public String addBookForm(){
        return "addBook";
    }

    @GetMapping("/books/add")
    public String addBook(@RequestParam String title, @RequestParam String author){
        bookService.addbook(title.trim(),author.trim().toUpperCase());
        return "redirect:/";
    }

    @GetMapping("/editbook")
    public String editBookForm(){
        return "editBook";
    }

    @GetMapping("/books/edit")
    public String editBook(@RequestParam Long id, @RequestParam String title, @RequestParam String author ){
        bookService.editbook(id,title,author);
        return "redirect:/";
    }
}
