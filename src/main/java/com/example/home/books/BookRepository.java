package com.example.home.books;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    List<BookDTO> findAllByTitle(String title);

    @Query("select new com.example.home.books.BookDTO(b.id,b.title,b.author) from Book b")
    List<BookDTO> findAllBook();

}
