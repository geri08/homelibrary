package com.example.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {


    @RequestMapping("/")
    public String showMainPage() {
        return "home";
    }

//    @RequestMapping("/Books")
//    public String showAllBooks(Model model) {
//        model.addAttribute()
//        return "books";
//    }

}
