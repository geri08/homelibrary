package com.example.home.user;

import org.springframework.stereotype.Service;

@Service
public class UserDAO {
    private UserRepository userRepository;

    public void saveUser(User user) {
        if (userRepository.findByName(user.getName()) == null) {
            userRepository.save(user);
        } else {
            throw new UserExistException("użytkownik istnieje");
        }
    }
}
