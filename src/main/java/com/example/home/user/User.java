package com.example.home.user;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Pattern(regexp = "[A-Z][a-zA-Z]{2,}", message = "podaj conajmniej 3 znaki od duzej litery")
    private String name;

    @Pattern(regexp = "[A-Z][a-z][\\w]{5,}", message = "duza litera, mala litera, ciag znakow min 5")
    private String password;
}
