package com.example.home.dataInit;

import com.example.home.books.Book;
import com.example.home.books.BookRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class Datainit implements ApplicationListener<ContextRefreshedEvent> {
    private BookRepository bookRepository;

    public Datainit(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        Book book1 = new Book();
        book1.setAuthor("Stephen King");
        book1.setTitle("reka mistrza");
        bookRepository.save(book1);


        Book book2 = new Book();
        book2.setAuthor("Stephen King");
        book2.setTitle("Misery");
        bookRepository.save(book2);


        Book book3 = new Book();
        book3.setAuthor("John Grisham");
        book3.setTitle("klient");
        bookRepository.save(book3);


        Book book4 = new Book();
        book4.setAuthor("remigiusz mroz");
        book4.setTitle("behawiorysta");
        bookRepository.save(book4);

    }
}
